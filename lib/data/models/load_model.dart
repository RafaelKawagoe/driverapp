class LoadModel {
  int total;

  LoadModel({required this.total});

  factory LoadModel.fromJson(Map<String, dynamic> json) {
    return LoadModel(
      total: json['total'],
    );
  }
}
