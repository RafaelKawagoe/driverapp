class DriverModel {
  String id;
  String firstName;
  String lastName;

  DriverModel({
    required this.id,
    required this.firstName,
    required this.lastName,
  });

  factory DriverModel.fromJson(Map<String, dynamic> json) {
    return DriverModel(
      id: json['ID'],
      firstName: json['first_name'],
      lastName: json['last_name'],
    );
  }
}
