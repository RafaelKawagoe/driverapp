class DependenciesModel {
  bool blocklisted;
  int boxes;
  List<int> orders;

  DependenciesModel({
    required this.blocklisted,
    required this.boxes,
    required this.orders,
  });

  factory DependenciesModel.fromJson(Map<String, dynamic> json) {
    return DependenciesModel(
        blocklisted: json['blocklisted'],
        boxes: json['boxes'],
        orders: json['orders'].cast<int>());
  }
}
