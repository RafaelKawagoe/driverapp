import 'package:driverapp/data/models/dependencies_model.dart';
import 'package:driverapp/data/models/driver_model.dart';
import 'package:driverapp/data/models/load_model.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:driverapp/data/service/http_service.dart';
import 'package:driverapp/locator.dart';

class DriverRepository implements DriverRepositoryInterface {
  final HttpService _httpService;
  final _baseUrl =
      'https://hmlg-logistics-mobile-driver-cnbi5ucx.ue.gateway.dev/api/v1';
  final token =
      'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5tZXJ5dG8uY29tLmJyLyIsImlhdCI6MTYzMzUzMjMwOSwibmJmIjoxNjMzNTMyMzA5LCJleHAiOjE2MzQxMzcxMDksImRhdGEiOnsidXNlciI6eyJpZCI6IjUwMCJ9fX0.cckvTtvCETxRSjqXKTWZeDNUDD-Ntc-nDVJ0H40x8dw';
  DriverRepository(this._httpService);

  @override
  Future<DependenciesModel?> getDriverPendencies(String driverId) async {
    String uri = '$_baseUrl/driver-pendency';
    var response = await _httpService.getRequest(uri, params: {
      'driver_id': driverId
    }, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.success) {
      try {
        return DependenciesModel.fromJson(response.content!);
      } catch (e) {
        return null;
      }
    }

    return null;
  }

  @override
  Future<DriverModel?> getDriverDetails() async {
    String uri = '$_baseUrl/users-me';
    var response = await _httpService.getRequest(uri, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    try {
      if (response.success) {
        var aux = DriverModel.fromJson(response.content!['data']);
        return aux;
      }
    } catch (e) {
      return null;
    }

    return null;
  }

  @override
  Future<LoadModel?> getLoad(String driverId) async {
    String uri = '$_baseUrl/load-truck';
    var response = await _httpService.getRequest(uri, params: {
      'driver_id': driverId
    }, headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $token',
    });
    if (response.success) {
      try {
        return LoadModel.fromJson(response.content!);
      } on Exception catch (e) {
        return null;
      }
    }

    return null;
  }
}
