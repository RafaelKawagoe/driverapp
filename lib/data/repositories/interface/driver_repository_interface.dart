import 'package:driverapp/data/models/dependencies_model.dart';
import 'package:driverapp/data/models/driver_model.dart';
import 'package:driverapp/data/models/load_model.dart';

abstract class DriverRepositoryInterface {
  Future<DriverModel?> getDriverDetails();
  Future<DependenciesModel?> getDriverPendencies(String driverId);
  Future<LoadModel?> getLoad(String driverId);
}
