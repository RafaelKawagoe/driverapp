import 'package:driverapp/data/repositories/driver_repository.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:get_it/get_it.dart';
import 'data/service/http_service.dart';

var getIt = GetIt.instance;

void setupLocator() {
  getIt.registerLazySingleton(() => HttpService());

  getIt.registerLazySingleton<DriverRepositoryInterface>(
      () => DriverRepository(getIt.get<HttpService>()));
}
