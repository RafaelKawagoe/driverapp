import 'package:driverapp/blocs/driver_info_bloc.dart';
import 'package:driverapp/blocs/driver_pendencies.dart';
import 'package:driverapp/blocs/load_cubit.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart';

import '../../locator.dart';

class DriverPage extends StatelessWidget {
  static const name = 'driver-page';

  const DriverPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => DriverinfoCubit(getIt.get<DriverRepositoryInterface>())
            ..getDriverinfo(),
        ),
        BlocProvider(
          create: (_) =>
              PendenciesCubit(getIt.get<DriverRepositoryInterface>()),
        ),
        BlocProvider(
          create: (_) => LoadCubit(getIt.get<DriverRepositoryInterface>()),
        )
      ],
      child: DriverView(),
    );
  }
}

class DriverView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          backgroundColor: Color(0xfff6f5f6),
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                onPressed: () {},
                icon: const Icon(Icons.menu),
                color: Color(0xff3c4558),
              ),
              const Text(
                'Facily Driver',
                style: TextStyle(color: Color(0xff3c4558)),
              ),
              IconButton(
                onPressed: () {
                  context.read<DriverinfoCubit>().getDriverinfo();
                },
                icon: const Icon(Icons.autorenew),
                color: Color(0xff3c4558),
              ),
            ],
          ),
        ),
        body:
            BlocBuilder<DriverinfoCubit, Driverinfo>(builder: (context, state) {
          if (state is LoadedDriverinfo) {
            context.read<PendenciesCubit>().getPendencies(state.pendencies.id);
            context.read<LoadCubit>().getLoad(state.pendencies.id);
            return SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: 10),
                    Text(
                      'Olá, ${state.pendencies.firstName} ${state.pendencies.lastName}',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    SizedBox(height: 5),
                    Text(
                      'Tudo bem?',
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                    ),
                    SizedBox(height: 20),
                    BlocBuilder<PendenciesCubit, Pendencies>(
                        builder: (context, state) {
                      return (state is LoadedPendencies)
                          ? _buildLoadAlert()
                          : Text('loading');
                    }),
                    SizedBox(height: 20),
                    BlocBuilder<LoadCubit, Load>(builder: (context, state) {
                      if (state is LoadedLoad) {
                        return _buildLoadOptions();
                      } else if (state is EmptyLoad) {
                        return SizedBox();
                      } else
                        return Text('loading');
                    }),
                    SizedBox(height: 20),
                    _buildUserId(state.pendencies.id),
                  ],
                ),
              ),
            );
          } else
            return Text('loading');
        }));
  }

  Widget _buildLoadAlert() {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xffa9e5ae),
          border: Border.all(
            color: Color(0xffa9e5ae),
          ),
          borderRadius: BorderRadius.all(Radius.circular(8))),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Carregamento liberado',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            SizedBox(height: 5),
            Text('Dirija-se ao setor responsável e faça um novo carregamento'),
            SizedBox(height: 5),
          ],
        ),
      ),
    );
  }

  Widget _buildLoadOptions() {
    return Container(
      width: 500,
      constraints: BoxConstraints(),
      decoration: BoxDecoration(
          color: Color(0xfff6f6f6),
          border: Border.all(
            color: Color(0xfff6f6f6),
          ),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const Text(
              'O que você deseja?',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
            ),
            const Text('selecione a opção abaixo'),
            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: const Color(0xff2a65f5),
                padding:
                    const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                textStyle:
                    const TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0)),
              ),
              child: Text(
                'Verificar carregamentos',
                textAlign: TextAlign.center,
              ),
              onPressed: () {},
            ),
            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xff2a65f5),
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                textStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0)),
              ),
              child: Text(
                'Entregar pedidos',
                textAlign: TextAlign.center,
              ),
              onPressed: () {},
            ),
            SizedBox(height: 20),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Color(0xff2a65f5),
                padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                textStyle: TextStyle(fontSize: 15, fontWeight: FontWeight.bold),
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50.0)),
              ),
              child: Text(
                'Consultar NFe',
                textAlign: TextAlign.center,
              ),
              onPressed: () {},
            ),
            SizedBox(height: 20),
          ],
        ),
      ),
    );
  }

  Widget _buildUserId(String id) {
    return Container(
      decoration: BoxDecoration(
          color: Color(0xfff6f6f6),
          border: Border.all(
            color: Color(0xfff6f6f6),
          ),
          borderRadius: BorderRadius.all(Radius.circular(5))),
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'Nº de indentiicação:',
                  style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500),
                ),
                SizedBox(height: 5),
                Text(
                  id,
                  style: TextStyle(fontSize: 23, fontWeight: FontWeight.bold),
                ),
                SizedBox(height: 15),
                SizedBox(
                  width: 140,
                  child: Text(
                    'Utilize o QRCode sempre que for necessário confirmar sua identificação',
                    style: TextStyle(fontSize: 13),
                  ),
                ),
              ],
            ),
            Image.network(
              "https://southamerica-east1-facily-817c2.cloudfunctions.net/generate_qr_code?data=$id",
              height: 140,
            )
          ],
        ),
      ),
    );
  }
}
