import 'package:driverapp/data/models/driver_model.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class Driverinfo {}

class LoadingDriverinfo extends Driverinfo {}

class LoadedDriverinfo extends Driverinfo {
  DriverModel pendencies;
  LoadedDriverinfo(this.pendencies);
}

class ErrorDriverinfo extends Driverinfo {}

class DriverinfoCubit extends Cubit<Driverinfo> {
  final DriverRepositoryInterface _driverRepository;
  DriverinfoCubit(this._driverRepository) : super(LoadingDriverinfo());

  Future<void> getDriverinfo() async {
    emit(LoadingDriverinfo());
    try {
      final pendencies = await _driverRepository.getDriverDetails();
      emit(LoadedDriverinfo(pendencies!));
    } catch (e) {
      emit(ErrorDriverinfo());
    }
  }
}
