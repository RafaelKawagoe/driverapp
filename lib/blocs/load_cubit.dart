import 'package:driverapp/data/models/dependencies_model.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class Load {}

class LoadingLoad extends Load {}

class LoadedLoad extends Load {}

class EmptyLoad extends Load {}

class ErrorLoad extends Load {}

class LoadCubit extends Cubit<Load> {
  final DriverRepositoryInterface _driverRepository;
  LoadCubit(this._driverRepository) : super(LoadingLoad());

  Future<void> getLoad(String id) async {
    emit(LoadingLoad());
    try {
      final pendencies = await _driverRepository.getLoad(id);
      (pendencies!.total != 0) ? emit(LoadedLoad()) : emit(EmptyLoad());
    } catch (e) {
      emit(ErrorLoad());
    }
  }
}
