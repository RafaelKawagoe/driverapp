import 'package:driverapp/data/models/dependencies_model.dart';
import 'package:driverapp/data/repositories/interface/driver_repository_interface.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

abstract class Pendencies {}

class LoadingPendencies extends Pendencies {}

class LoadedPendencies extends Pendencies {}

class EmptyPendencies extends Pendencies {}

class ErrorPendencies extends Pendencies {}

class PendenciesCubit extends Cubit<Pendencies> {
  final DriverRepositoryInterface _driverRepository;
  PendenciesCubit(this._driverRepository) : super(LoadingPendencies());

  Future<void> getPendencies(String id) async {
    emit(LoadingPendencies());
    try {
      final pendencies = await _driverRepository.getDriverPendencies(id);
      (pendencies!.orders.isEmpty && pendencies.boxes == 0)
          ? emit(EmptyPendencies())
          : emit(LoadedPendencies());
    } catch (e) {
      emit(ErrorPendencies());
    }
  }
}
