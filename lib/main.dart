import 'package:driverapp/locator.dart';
import 'package:driverapp/ui/pages/driver_page.dart';
import 'package:flutter/material.dart';

void main() {
  setupLocator();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Driver App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: DriverPage.name,
      routes: {
        DriverPage.name: (_) => DriverPage(),
      },
    );
  }
}
